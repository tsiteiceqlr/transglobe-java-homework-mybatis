package com.tgl.mybatis.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.tgl.mybatis.dao.EmployeeDao;
import com.tgl.mybatis.model.Employee;
import com.tgl.mybatis.util.DataUtil;

@Service
public class EmployeeService {

	@Autowired
	private EmployeeDao employeeDao;

	public Integer insert(Employee employee) {
		employee.setBmi(DataUtil.bmi(employee.getHeight(), employee.getWeight()));
		return employeeDao.insert(employee);
		
	}

	public boolean delete(int employeeId) {
		return employeeDao.delete(employeeId);
	}
	
	public boolean update(Employee employee) {
		employee.setBmi(DataUtil.bmi(employee.getHeight(), employee.getWeight()));
		return employeeDao.update(employee);
	}

	public Employee findById(int employeeId) {
		Employee result = employeeDao.findById(employeeId);
		if (result == null) {
			return null;
		}
		String maskedName = DataUtil.maskChineseName(result.getChineseName());
		result.setChineseName(maskedName);
		return result;
	}

}
