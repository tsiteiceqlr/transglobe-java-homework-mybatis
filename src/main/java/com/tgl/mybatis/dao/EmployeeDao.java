package com.tgl.mybatis.dao;

import org.springframework.stereotype.Repository;

import com.tgl.mybatis.model.Employee;

@Repository
public interface EmployeeDao {
	
	Employee findById(int id);
	
	int insert(Employee employee);
	
	boolean delete(int id);
	
	boolean update(Employee employee);

}
